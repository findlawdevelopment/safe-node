#Safe-Node
A small module to help provide safe integration into express js apps.

[TOC]

##Getting Started
To get started using `safe-node`, you'll need to install it:

    npm install --save git+https://bitbucket.org/findlawdevelopment/safe-node.git

Then include it in your project

    var safe = require("safe-node");

When you are first initializing your app, you will need to make sure the following properties are set on your express app:
* `safeURL`
* `secretKey`

You can do this by:

    safe.set("safeURL", "https://...");
    safe.set("secretKey", "...");

You will also want to use some form of post data parsing:
* `app.use(express.bodyParser());` or
* `app.use(express.urlencoded());` or
* `app.use(express.json());`

Ideally, you"ll want different values based on your qa and production environments. See [app configuration](http://expressjs.com/api.html#app.configure) in the express documentation

Now when you are declaring your view, wrap your view function like this

    function (req, res) {
      return safe(function (req, res, error) {
        // View function here
      }, req, res)
    }

Lets break that down a bit...

###safe(callBack, req, res)
This function wraps your express view and figures out if the user is currently signed in to safe. If they are, it calls `callBack` with the `req`, and `res` objects from express. If there is an error, it will call `callBack` function with `req`, `res`, and an `error` message.

####callBack
This is your view function. When everything goes right, it will be called with the `req` and `res` objects as if the safe funciton wasn"t even there.

If a digest property isn't found, `callBack` won't be called at all. instead this function will be called:

    res.redirect(app.get("safeURL"));


If something goes wrong when evaluating the SAFE data, `callBack` will be called as such.

Digest doesn't match:

    callBack(req, res, "invalid digest")

Time of safe request is 5 minutes before current time:

    callBack(req, res, "authentication expired")


If everything goes right, `callBack` will be called as such:

    callBack(req, res);

To test if something went wrong with your safe authentication, add this to your view function:

    function (req, res) {
      return safe(function (req, res, error) {

        if (error) {
            res.send(500, { error: error });
        }

      }, req, res)
    }


####req
This is the request object from express. It holds the post data needed to validate SAFE

####res
This is the response object from express. It's needed to automatically redirect the user if digest isn't a property in the post data.
