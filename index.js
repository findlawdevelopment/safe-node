var crypto = require('crypto');
var moment = require('moment-timezone');

var _data = _data || {};

function Safe (callback, req, res) {
  /*
    callback - view function for when things go right

    req - the express request from the url

    res - the express response to the view
  */

  if (req.headers['user-agent'].indexOf('flcrawler') > -1) {
    // detect the smoke test crawler and return 200 (app is running)
    return res.status(200).send();
  }

  // look for safe url
  if (!Safe.get('safeURL')) {
    var err = 'safeURL is not defined.';
    return callback(req, res, err);
  }

  // look for secret key
  if (!Safe.get('secretKey')) {
    var err = 'secretKey is not defined.';
    return callback(req, res, err);
  }

  var path = req.route.path,
      url = Safe.get('safeURL');

  if (path) {
    url += (url.indexOf('?') >= 0 ? '&' : '?') + 'siteLink=' + path;
  }

  if (!req.body.digest) {
    return res.redirect(url);
  }

  // Set request time to use GMT
  var reqTime = moment(req.body.time + ' +0000', 'YYYY:MM:DD:HH:mm:ss Z'),
      // Compare current time to POST time
      elapsed = moment().diff(reqTime);

  var md5 = crypto.createHash('md5'),
      keys = [req.body.uid, req.body.time, Safe.get('secretKey')];

  md5.update(keys.join(''), 'utf8');

  var hash = md5.digest('hex');


  if (hash === req.body.digest) {
    // Check if the safe calculated digest matches our digest using the md5 algorithm

    if (elapsed < 300000) {
    // Check if it's been 5 minutes (300000 milliseconds) from the proposed request time to now

      return callback(req, res);
      
    } else {

      return callback(req, res, 'authentication expired');

    }

  } else {

    return callback(req, res, 'invalid digest');
    
  }

}

Safe.get = function (key) {
  return _data[key];
};

Safe.set = function (key, value) {
  _data[key] = value;
  return value;
};

module.exports = Safe;
