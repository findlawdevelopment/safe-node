var safe = require('../../index'),
    crypto = require('crypto'),
    makeSafeDate = require('../makeSafeDate');

safe.set('safeURL', 'https://safeqa.thomson.com/login/sso/SSOService?app=checkMaster_qa');
safe.set('secretKey', 'Ch9410lCfr34Sd20');

var res = {
  // fake express response to spy on redirect
  'redirect': function (url) {
    console.log('redirecting to: ' + url);
  },
  'status': function (arg) {
    res.resStatus = arg;
    return res;
  },
  'send': function () {
    console.log('sending response' + res.resStatus);
  }
};

var callBack = function (req, res, error) {
  // function that is called after the safe function has done it's work

  if (error) {
    return error;
  }

  return true;
};

var createHashString = function (alg, keys) {
  // easy way to make a new hash digest
  
  var hash = crypto.createHash(alg);
  hash.update(keys.join(''), 'utf8');
  return hash.digest('hex');

};

var req;


describe('safe node module', function () {
    beforeEach(function () {

      req = {
        "body": {
          "uid": 'u1234567',
          "digest": '',
          "time": makeSafeDate()
        },
        "route": {
          "path": ""
        },
        headers: {
          'user-agent': ''
        }
      };
        
    });

    afterEach(function () {
        safe.set('safeURL', 'https://safeqa.thomson.com/login/sso/SSOService?app=checkMaster_qa');
        safe.set('secretKey', 'Ch9410lCfr34Sd20');
    });

    it('attempts to redirect to the safe url', function () {
      var redirectSpy = spyOn(res, 'redirect');

      safe(callBack, req, res);

      expect(redirectSpy).toHaveBeenCalledWith(safe.get('safeURL'));
    });

    it('fails from bad digest', function () {
      req.body.digest = 'This is a bad digest';

      expect(safe(callBack, req, res)).toBe('invalid digest');
    });

    it('fails from being past 5 minutes', function () {
      var d = new Date();
      d.setMinutes(d.getMinutes() - 6);

      req.body.time = makeSafeDate(d);

      req.body.digest = createHashString('md5', [req.body.uid, req.body.time, safe.get('secretKey')]);

      expect(safe(callBack, req, res)).toBe('authentication expired');
    });


    it('passes', function () {
      req.body.digest = createHashString('md5', [req.body.uid, req.body.time, safe.get('secretKey')]);

      expect(safe(callBack, req, res)).toBe(true);
    });

    it('redirect to the requested url', function () {
      req.route.path = "http://localhost:3092/#?url=nmmmn.firmsitepreview.com";
      
      var redirectSpy = spyOn(res, 'redirect');

      safe(callBack, req, res);

      expect(redirectSpy).toHaveBeenCalledWith(safe.get('safeURL') + '&siteLink=' + req.route.path);
    });

    it('detects the flcrawler and sends 200', function () {
      req.headers['user-agent'] = 'flcrawler';

      var sendSpy = spyOn(res, 'send');

      safe(callBack, req, res);

      expect(sendSpy).toHaveBeenCalled();
    });

    describe('set/get methods', function () {
      it('saves/retrieves the data passed', function () {
        expect(safe.set('foo', 'bar')).toBe('bar');
        expect(safe.get('foo')).toBe('bar');
      });
    });
});