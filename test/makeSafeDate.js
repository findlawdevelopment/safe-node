var moment = require('moment');

module.exports = function (date) {

 /* var getZeroBased = function (d, time) {
    var method = 'get' + time;
    return d[method]().toString().length === 1 ? '0' + (d[method]() + 1) : d[method]() + 1;
  };

  var getHours = function (d) {
    var mHours = d.getHours();
    var hours = mHours > 12 ? mHours - 12 : mHours;
    return hours < 10 ? '0' + hours : hours;
  };

  var getMilliSeconds = function (d) {
    var ms = d.getMilliseconds();
    ms = ms > 99 ? parseInt(ms / 10, 10) : ms; // make 2 digits from 3
    ms = ms < 10 ? '0' + ms : ms; // make 2 digits from 1
    return ms;
  };

*/

  var d = date || new Date();
  return moment(d).utc().format('YYYY:MM:DD:HH:mm:ss');
};