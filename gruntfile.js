module.exports = function(grunt){
    grunt.loadNpmTasks('grunt-jasmine-node');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
          dist: {
            files: {
              'index.js': 'src/index.js',
              'safe-date.js': 'src/safe-date.js'
            }
          }
        },
        jasmine_node: {
          all: ['test/specs/']
        }
    });

    grunt.registerTask('default', ['jasmine_node']);

};
